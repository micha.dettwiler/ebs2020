﻿/**@file*/
#include "Time.h"
#include <stdio.h>
	
/**
 * @brief increments the time  by one second 
 *
 * @param[in] none
 *
 * @return returns 1 in case of a day overflow, 0 else
 */
int Time::Tick(void){
	int ret = 0;
	ss_ = (ss_ + 1) % 60; // Sekunden
	if (ss_ == 0) {
		mm_ = (mm_ + 1) % 60;  // Minuten
		if (mm_ == 0) {
			hh_ = (hh_ + 1) % 24; // Stunden
			if (hh_ == 0) ret = 1;
		}
	}
	return ret;
}
/**
 * @brief change time to the new time
 * 
 * @param[in] z:	struct, new time
 * 
 * @return void
 */
void Time::Change(Time &z){
	hh_= z.GetHours();
	mm_= z.GetMinutes();
	ss_= z.GetSeconds();
}
int Time::GetHours(){return hh_;};
int Time::GetMinutes(){return mm_;};
int Time::GetSeconds(){return ss_;};
int Time::GetSet(){return set_;};
/**
 * @brief add s to the current hour and test ist
 * 
 * @param[in] h:	int, new hour
 * 
 * @return void
 */
void Time::ChangeHour(int h)
{
	hh_=(hh_+h)%24;
	if(hh_<0) hh_=23;
	ss_ = 0;
}
/**
 * @brief add s to the current minute and test ist
 * 
 * @param[in] m:	int, new minute
 * 
 * @return void
 */
void Time::ChangeMin(int m)
{
	mm_=(mm_+m)%60;
	if(mm_==-1) mm_=59;
	ss_ = 0;

}
void Time::setSet(int s){set_=s;};
/**
 * @brief add s to the current second and test ist
 * 
 * @param[in] s:	int, new seconde
 * 
 * @return void
 */
void Time::ChangeSet(int s)
{
	set_+=s;
	if ((set_ < 1) || (set_ > 2))  set_ = 1;
}
/**
 * @brief add h to the current hour and test ist
 * 
 * @param[in] h:	int, new hour
 * 
 * @return void
 */
int Time::addHour(int h)
{
	int ret = 0;
	if(hh_ + h >= 24) 
		ret = 1;
	else if (hh_ + h < 0)
		ret = -1;
	// ACHTUNG Modulo gibt nur für positive Werte ein gutes ergebnis	
	hh_ = (((hh_ + h) % 24) + 24) % 24; // Stunden
	
	return ret;
}
/**
 * @brief set the Time to h,m,s
 * 
 * @param[in] h:	int, new hour
 * 
 * @return void
 */
void Time::setTime(int h,int m,int s)
{
	hh_=h;
	mm_=m;
	ss_=s;
}

/**
 * @brief print the given time in a "hh:mm:ss"-frame in the String text (24h Format)
 * 
 * @param[in] text:	pointer to an char[16], return String for lcd
 * 
 * @return void
 */

void Time::printTime24(char* text)
{
	sprintf(text,"%02u:%02u:%02u", hh_,
	mm_,ss_);	
}
/**
 * @brief print the given time in a "hh:mm:ss"-frame in the String text (12h Format)
 * 
 * @param[in] text:	pointer to an char[16], return String for lcd
 * 
 * @return void
 */
void Time::printTime12(char* text)
{
	if (hh_ < 12)
		if (hh_ < 1)
			sprintf(text,"%02u:%02u:%02u AM", hh_ + 12,
			mm_,ss_);
		else
			sprintf(text,"%02u:%02u:%02u AM", hh_,
			mm_,ss_);
	else
		if (hh_ <= 12)
			sprintf(text,"%02u:%02u:%02u PM", hh_,
			mm_,ss_);
		else
			sprintf(text,"%02u:%02u:%02u PM", hh_ -12,
			mm_,ss_);
}