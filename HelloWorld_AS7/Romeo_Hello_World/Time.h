

#ifndef _TIME33_h
#define _TIME33_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


class Time 
{
	int hh_;
	int mm_;
	int ss_;
	int set_;
	public:
	Time(int h, int m, int s){ hh_=h; mm_=m; ss_=s; set_ =1;};
	void Change(Time &z);
	int Tick();
	int GetHours();
	int GetMinutes();
	int GetSeconds();
	int GetSet();
	void ChangeHour(int h);
	void ChangeMin(int m);
	void ChangeSec(int s);
	void ChangeSet(int s);
	void setSet(int s);
	int addHour(int h);
	void setTime(int h,int m,int s);
	void printTime24(char* text);
	void printTime12(char* text);
};
#endif
