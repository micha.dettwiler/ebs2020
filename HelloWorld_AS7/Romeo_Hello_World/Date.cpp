﻿/**@file*/
#include "Date.h"

/**
 * @brief increments the calendar date by one day 
 *
 * @return none
 */
void Date::Tick(void){
	DD_ += 1;  // Tage
	if (DD_ > DaysOfMonth()) {
		DD_ = 1;
		MM_ += 1; // Monate
		if (MM_ > 12) {
			MM_ = 1;
			YYYY_ += 1; // Jahre
		}
	}
}
/**
 * @brief calculates the days of a month in a year
 *
 * returns 28,29,30 or 31 in accordance to month and year. every 4th year is considered a leap year. No correction dates
 * are taken into account
 *
 * @param[in] d struct Datum_S,
 * 
 * @return int, 28,29,30 or 31 in accordance to month and year
 */
int Date::DaysOfMonth(){
	switch (MM_) {
	case 2:
		if (YYYY_ % 4 != 0) return 28;
		else return 29;
	case 4: case 6: case 9: case 11:
		return 30;
	default:
		return 31;
	}
}
/**
 * @brief returns the Year
 *
 * @return YYYY_: int, The year of the Date
 */
int Date::GetYear(){return YYYY_;};
	/**
 * @brief returns the Month
 *
 * @return MM_: int, The month of the Date
 */
int Date::GetMonth(){return MM_;};
	/**
 * @brief return the Day
 *
 * @return DD_: int, The day of the Date
 */
int Date::GetDay(){return DD_;};
	/**
 * @brief return the value of set
 *
 * @return set_: int, The value of set
 */
int Date::getSet(){return set_;};
/**
 * @brief Add the input to the current year and test the new value
 *
 * @para[in] y: int, The new year
 *
 * @return void
 */
void Date::ChangeYear(int y)
{
	YYYY_+= y;
	if(DD_>DaysOfMonth()) DD_=DaysOfMonth();
}
/**
 * @brief Add the input to the current month and test the new value
 *
 * @para[in] m: int, The new month
 *
 * @return void
 */
void Date::ChangeMonth(int m)
{
	MM_+=m;
	if(DD_>DaysOfMonth()) DD_=DaysOfMonth();
	if(MM_>12)	MM_=1;
	if(MM_==0) MM_=12;	
}
/**
 * @brief Add the input to the current day and test the new value
 *
 * @para[in] d: int, The value to be added to the current day
 *
 * @return void
 */
void Date::ChangeDay(int d)
{
	DD_+=d;
	if(DD_>DaysOfMonth()) DD_=1;
	if(DD_==0) DD_=DaysOfMonth();
}
/**
 * @brief Change set
 *
 * @para[in] s: int, The new value for set
 *
 * @return void
 */
void Date::setSet(int s){set_=s;};
	/**
 * @brief Add the input to the current day and test the new value
 *
 * @para[in] s: int, he value to be added to the current set
 *
 * @return void
 */
void Date::ChangeSet(int s)
{
	set_+=s;
	if ((set_ <= 0) || (set_ > 3))  set_ = 1;
}
/**
 * @brief Change the Date
 *
 * @para[in] z: Date, The new date
 *
 * @return void
 */	
void Date::Change(Date &z)
{
	DD_= z.GetDay();
	MM_= z.GetMonth();
	YYYY_= z.GetYear();
}
/**
 * @brief Add the input to the current day and test the new value
 *
 * @para[in] d: int, The value to be added to the current day
 *
 * @return void
 */
void Date::addDay(int d)
{
	DD_ += d;  // Tage
	if (DD_ > DaysOfMonth()) {
		DD_ = 1;
		MM_ += 1; // Monate
		if (MM_ > 12) {
			MM_ = 1;
			YYYY_ += 1; // Jahre
		}
	} else if (DD_ < 1) {
		if((--MM_) < 1) {
			 MM_=12;
			 YYYY_--;
		}
		DD_= DaysOfMonth();
	}
}
/**
 * @brief Change the Date
 *
 * @para[in] y: int, The new year
			m: int, The new month
			d: int, The new day
 *
 * @return void
 */	
void Date::setDate(int y, int m, int d)
{
	YYYY_=y;
	MM_=m;
	DD_=d;
}
/**
* @brief return the given date in a "DD.MM.YYYY"-frame in the String text 
*
* @param[in] text:	pointer to an char[16], return String for lcd
*
* @return void
*/

void Date::print(char* text)
{
	sprintf(text,"%02u.%02u.%02u", DD_,
	MM_,YYYY_);
}