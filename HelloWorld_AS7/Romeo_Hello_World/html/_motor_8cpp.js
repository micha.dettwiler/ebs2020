var _motor_8cpp =
[
    [ "encoderMotor", "_motor_8cpp.html#a2573a53f592540fd34128539cc38ff9a", null ],
    [ "initMotor", "_motor_8cpp.html#a72af49fb7191c300fa1a1a8ec5c91b13", null ],
    [ "MoveMotor", "_motor_8cpp.html#a26da311dffb737667e76f0db95dc0b17", null ],
    [ "resetEncoder", "_motor_8cpp.html#a812a103dc597b989ec857571a2e3a47c", null ],
    [ "TimeToImp", "_motor_8cpp.html#a2a6d7cabb81f6f6a64b4b55d724d1cef", null ],
    [ "clockEnable", "_motor_8cpp.html#a12940d3eb6f47be4350642c409675b8e", null ],
    [ "encodeCount", "_motor_8cpp.html#a727d84a9488f35836e45cb2ae1a11edb", null ],
    [ "inputValue", "_motor_8cpp.html#a5fa499d28ddd79c2e722bb0442ee2d5d", null ],
    [ "measurementValue", "_motor_8cpp.html#a37cb13908a8a1d7a8cf5827aa3d2fe17", null ],
    [ "ref", "_motor_8cpp.html#a454d4b81256be362313ed296455e5a71", null ],
    [ "ref1", "_motor_8cpp.html#a2585ffd2a97ae2f0137f610f3e1dbf10", null ],
    [ "ref2", "_motor_8cpp.html#a7aba56bf557e12b2abc70247d4a40050", null ],
    [ "referenceValue", "_motor_8cpp.html#a9de45f5ac23e3463b6a5ee2add5b5e5f", null ],
    [ "speed2", "_motor_8cpp.html#ad7a9c52e9e63d8d53b51b14028d5cd65", null ],
    [ "targetCount", "_motor_8cpp.html#a72dbfe614aeadc7de8e3cfda8b2171d2", null ],
    [ "targetSpeed", "_motor_8cpp.html#ab0efc5200bc94ebb43f4566b239df6f2", null ],
    [ "test", "_motor_8cpp.html#a11443869853383169dd8ca87d79ddfa8", null ],
    [ "test1", "_motor_8cpp.html#ae7152d015acfd74e3445d9c21f7e741a", null ],
    [ "test2", "_motor_8cpp.html#a6acf08cdb0566d959f3fad97755bbe2d", null ]
];