var searchData=
[
  ['dht_5fbus_5fhung_508',['DHT_BUS_HUNG',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97a71a0800cb05a5a47092ccb326abdaa87',1,'DHT22.h']]],
  ['dht_5ferror_5fack_5ftoo_5flong_509',['DHT_ERROR_ACK_TOO_LONG',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97a1d764bbe339fa7ffa6486eaa2d34b3a7',1,'DHT22.h']]],
  ['dht_5ferror_5fchecksum_510',['DHT_ERROR_CHECKSUM',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97aaf901eb3372a7b49c1561ce2ab5b666d',1,'DHT22.h']]],
  ['dht_5ferror_5fdata_5ftimeout_511',['DHT_ERROR_DATA_TIMEOUT',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97a429361c408ffdbc9a24ef69f34670262',1,'DHT22.h']]],
  ['dht_5ferror_5fnone_512',['DHT_ERROR_NONE',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97a31a1f2b2d08ad438ed41b39c61f8fcd7',1,'DHT22.h']]],
  ['dht_5ferror_5fnot_5fpresent_513',['DHT_ERROR_NOT_PRESENT',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97a5f7db7c5ebdb11b8caac59ba13b84c61',1,'DHT22.h']]],
  ['dht_5ferror_5fsync_5ftimeout_514',['DHT_ERROR_SYNC_TIMEOUT',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97aa7c9b7ffdf6f802004713a2953a50f04',1,'DHT22.h']]],
  ['dht_5ferror_5ftooquick_515',['DHT_ERROR_TOOQUICK',['../_d_h_t22_8h.html#a2a445c881279fc2d0e42bdc269970d97a63a5a4ecc0d806847904e2b7c0f8a3d7',1,'DHT22.h']]]
];
