var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuwxz",
  1: "cdfgpt",
  2: "degmprst",
  3: "acdegilmprstuwz",
  4: "abcdeghilmnopqrstuw",
  5: "ipu",
  6: "d",
  7: "d",
  8: "_abcdeiklmnoprsux"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

