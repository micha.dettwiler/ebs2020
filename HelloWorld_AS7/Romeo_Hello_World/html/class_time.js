var class_time =
[
    [ "Time", "class_time.html#ac6fb82532bbcc0b10f17cc38accd5cab", null ],
    [ "addHour", "class_time.html#a47e797d8466779b591976ead88535bcf", null ],
    [ "Change", "class_time.html#ad385ba0fdf10d639da25038b3886af45", null ],
    [ "ChangeHour", "class_time.html#a1434d245e4481be81e1191e7521ae1e8", null ],
    [ "ChangeMin", "class_time.html#ab0976ad961422854dd29f5aa14664bc2", null ],
    [ "ChangeSec", "class_time.html#a750ff6a2106a5890ce86bc3010a47571", null ],
    [ "ChangeSet", "class_time.html#a3b661b25d9d0759b1bf1fee8699e6cbd", null ],
    [ "GetHours", "class_time.html#aee6d09b02fcf86ff620461823faa07cf", null ],
    [ "GetMinutes", "class_time.html#a7048a3139246f042d2c900d010ce76c2", null ],
    [ "GetSeconds", "class_time.html#a385ac4242fbc62cf554dc75d93fc64b1", null ],
    [ "GetSet", "class_time.html#a8a2977c92a14a68e0228e98d85e02c32", null ],
    [ "printTime12", "class_time.html#ab25f8f525228947e5659bfd35f65583b", null ],
    [ "printTime24", "class_time.html#a50b2f6cb2c28e1b23620b8bd93484e3c", null ],
    [ "setSet", "class_time.html#a6087a92b9d66060b36a50a44a8d87469", null ],
    [ "setTime", "class_time.html#a6d076211c852ba001617f2bd863013d4", null ],
    [ "Tick", "class_time.html#a2248cce9e44ca80f19f223fabe828491", null ]
];