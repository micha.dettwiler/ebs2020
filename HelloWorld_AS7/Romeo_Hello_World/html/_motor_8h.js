var _motor_8h =
[
    [ "COUNTS_PER_SECOND", "_motor_8h.html#a9a54de62314f5da149c59c51df5c4197", null ],
    [ "K_D", "_motor_8h.html#a0b9e481775b5a7ecdd0dbb43cacc1a09", null ],
    [ "K_I", "_motor_8h.html#a5e606c8dc6de900bea3cd7740ac1bd4d", null ],
    [ "K_P", "_motor_8h.html#acfc98b8c69707c30ac9a96bb12f2a0ab", null ],
    [ "LOCAL_TIMEZONE", "_motor_8h.html#ad3032764d7a1952b2ca4ff4e84ad11b5", null ],
    [ "MAX_COUNTS", "_motor_8h.html#adab823450ed588221b47f7794837e2b2", null ],
    [ "encoderMotor", "_motor_8h.html#a2573a53f592540fd34128539cc38ff9a", null ],
    [ "initMotor", "_motor_8h.html#a72af49fb7191c300fa1a1a8ec5c91b13", null ],
    [ "MoveMotor", "_motor_8h.html#a26da311dffb737667e76f0db95dc0b17", null ],
    [ "resetEncoder", "_motor_8h.html#a812a103dc597b989ec857571a2e3a47c", null ],
    [ "TimeToImp", "_motor_8h.html#a2a6d7cabb81f6f6a64b4b55d724d1cef", null ],
    [ "buttonPin", "_motor_8h.html#a4ddb8b6ae564eb22f7c74f2683a63b8e", null ],
    [ "enableDrivePin", "_motor_8h.html#a534b446e0df0e8551308d36a63509157", null ],
    [ "invertDrivePin", "_motor_8h.html#a48681a9f6128a6359d05b0e1b2bc607b", null ]
];