/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"Data Structures",url:"annotated.html",children:[
{text:"Data Structures",url:"annotated.html"},
{text:"Data Structure Index",url:"classes.html"},
{text:"Data Fields",url:"functions.html",children:[
{text:"All",url:"functions.html",children:[
{text:"a",url:"functions.html#index_a"},
{text:"c",url:"functions.html#index_c"},
{text:"d",url:"functions.html#index_d"},
{text:"e",url:"functions.html#index_e"},
{text:"g",url:"functions.html#index_g"},
{text:"h",url:"functions.html#index_h"},
{text:"l",url:"functions.html#index_l"},
{text:"o",url:"functions.html#index_o"},
{text:"p",url:"functions.html#index_p"},
{text:"r",url:"functions.html#index_r"},
{text:"s",url:"functions.html#index_s"},
{text:"t",url:"functions.html#index_t"},
{text:"u",url:"functions.html#index_u"}]},
{text:"Functions",url:"functions_func.html",children:[
{text:"a",url:"functions_func.html#index_a"},
{text:"c",url:"functions_func.html#index_c"},
{text:"d",url:"functions_func.html#index_d"},
{text:"e",url:"functions_func.html#index_e"},
{text:"g",url:"functions_func.html#index_g"},
{text:"p",url:"functions_func.html#index_p"},
{text:"r",url:"functions_func.html#index_r"},
{text:"s",url:"functions_func.html#index_s"},
{text:"t",url:"functions_func.html#index_t"}]},
{text:"Variables",url:"functions_vars.html"}]}]},
{text:"Files",url:"files.html",children:[
{text:"File List",url:"files.html"},
{text:"Globals",url:"globals.html",children:[
{text:"All",url:"globals.html",children:[
{text:"_",url:"globals.html#index__5F"},
{text:"a",url:"globals_a.html#index_a"},
{text:"b",url:"globals_b.html#index_b"},
{text:"c",url:"globals_c.html#index_c"},
{text:"d",url:"globals_d.html#index_d"},
{text:"e",url:"globals_e.html#index_e"},
{text:"g",url:"globals_g.html#index_g"},
{text:"i",url:"globals_i.html#index_i"},
{text:"k",url:"globals_k.html#index_k"},
{text:"l",url:"globals_l.html#index_l"},
{text:"m",url:"globals_m.html#index_m"},
{text:"n",url:"globals_n.html#index_n"},
{text:"o",url:"globals_o.html#index_o"},
{text:"p",url:"globals_p.html#index_p"},
{text:"q",url:"globals_q.html#index_q"},
{text:"r",url:"globals_r.html#index_r"},
{text:"s",url:"globals_s.html#index_s"},
{text:"t",url:"globals_t.html#index_t"},
{text:"u",url:"globals_u.html#index_u"},
{text:"w",url:"globals_w.html#index_w"},
{text:"x",url:"globals_x.html#index_x"},
{text:"z",url:"globals_z.html#index_z"}]},
{text:"Functions",url:"globals_func.html",children:[
{text:"c",url:"globals_func.html#index_c"},
{text:"e",url:"globals_func.html#index_e"},
{text:"g",url:"globals_func.html#index_g"},
{text:"i",url:"globals_func.html#index_i"},
{text:"l",url:"globals_func.html#index_l"},
{text:"m",url:"globals_func.html#index_m"},
{text:"p",url:"globals_func.html#index_p"},
{text:"r",url:"globals_func.html#index_r"},
{text:"s",url:"globals_func.html#index_s"},
{text:"t",url:"globals_func.html#index_t"},
{text:"u",url:"globals_func.html#index_u"},
{text:"w",url:"globals_func.html#index_w"},
{text:"z",url:"globals_func.html#index_z"}]},
{text:"Variables",url:"globals_vars.html",children:[
{text:"b",url:"globals_vars.html#index_b"},
{text:"c",url:"globals_vars.html#index_c"},
{text:"e",url:"globals_vars.html#index_e"},
{text:"g",url:"globals_vars.html#index_g"},
{text:"i",url:"globals_vars.html#index_i"},
{text:"l",url:"globals_vars.html#index_l"},
{text:"m",url:"globals_vars.html#index_m"},
{text:"n",url:"globals_vars.html#index_n"},
{text:"p",url:"globals_vars.html#index_p"},
{text:"q",url:"globals_vars.html#index_q"},
{text:"r",url:"globals_vars.html#index_r"},
{text:"s",url:"globals_vars.html#index_s"},
{text:"t",url:"globals_vars.html#index_t"},
{text:"w",url:"globals_vars.html#index_w"}]},
{text:"Typedefs",url:"globals_type.html"},
{text:"Enumerations",url:"globals_enum.html"},
{text:"Enumerator",url:"globals_eval.html"},
{text:"Macros",url:"globals_defs.html",children:[
{text:"_",url:"globals_defs.html#index__5F"},
{text:"a",url:"globals_defs.html#index_a"},
{text:"b",url:"globals_defs.html#index_b"},
{text:"c",url:"globals_defs.html#index_c"},
{text:"d",url:"globals_defs.html#index_d"},
{text:"e",url:"globals_defs.html#index_e"},
{text:"i",url:"globals_defs.html#index_i"},
{text:"k",url:"globals_defs.html#index_k"},
{text:"l",url:"globals_defs.html#index_l"},
{text:"m",url:"globals_defs.html#index_m"},
{text:"n",url:"globals_defs.html#index_n"},
{text:"o",url:"globals_defs.html#index_o"},
{text:"p",url:"globals_defs.html#index_p"},
{text:"r",url:"globals_defs.html#index_r"},
{text:"s",url:"globals_defs.html#index_s"},
{text:"u",url:"globals_defs.html#index_u"},
{text:"x",url:"globals_defs.html#index_x"}]}]}]}]}
