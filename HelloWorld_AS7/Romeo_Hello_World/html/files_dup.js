var files_dup =
[
    [ "Date.cpp", "_date_8cpp.html", null ],
    [ "Date.h", "_date_8h.html", [
      [ "Date", "class_date.html", "class_date" ]
    ] ],
    [ "DHT22.cpp", "_d_h_t22_8cpp.html", "_d_h_t22_8cpp" ],
    [ "DHT22.h", "_d_h_t22_8h.html", "_d_h_t22_8h" ],
    [ "Display.h", "_display_8h.html", "_display_8h" ],
    [ "Encoder.cpp", "_encoder_8cpp.html", "_encoder_8cpp" ],
    [ "Encoder.h", "_encoder_8h.html", "_encoder_8h" ],
    [ "GPS.cpp", "_g_p_s_8cpp.html", null ],
    [ "GPS.h", "_g_p_s_8h.html", "_g_p_s_8h" ],
    [ "Motor.cpp", "_motor_8cpp.html", "_motor_8cpp" ],
    [ "Motor.h", "_motor_8h.html", "_motor_8h" ],
    [ "PID_v1.cpp", "_p_i_d__v1_8cpp.html", null ],
    [ "PID_v1.h", "_p_i_d__v1_8h.html", "_p_i_d__v1_8h" ],
    [ "Romeo_keys.cpp", "_romeo__keys_8cpp.html", "_romeo__keys_8cpp" ],
    [ "Romeo_keys.h", "_romeo__keys_8h.html", "_romeo__keys_8h" ],
    [ "Sketch.cpp", "_sketch_8cpp.html", "_sketch_8cpp" ],
    [ "Stdint.h", "_stdint_8h.html", "_stdint_8h" ],
    [ "Time.cpp", "_time_8cpp.html", null ],
    [ "Time.h", "_time_8h.html", [
      [ "Time", "class_time.html", "class_time" ]
    ] ]
];