var _encoder_8h =
[
    [ "ENC_DDR", "_encoder_8h.html#a3a43eba195855fa5f87077df7f16eb21", null ],
    [ "ENC_PIN", "_encoder_8h.html#ab6d60004954f52bde5890a4b4679c03e", null ],
    [ "ENC_Q1", "_encoder_8h.html#ab83e36c59f528f8259bd43eabc0cf29b", null ],
    [ "ENC_Q2", "_encoder_8h.html#ad9dc586d14b0569f0c67e300a4755798", null ],
    [ "ENC_Q3", "_encoder_8h.html#adfaacb79cfd7e5ef81c52580c75cd6de", null ],
    [ "ENC_Q4", "_encoder_8h.html#ab214b58f029a9ce2ad2196e1c672f6bc", null ],
    [ "ENC_SIGA", "_encoder_8h.html#a34fdd5f24d67818900a46a62f486dd3e", null ],
    [ "ENC_SIGB", "_encoder_8h.html#a9953bd1ee7bd4a6f4a528b1ad98c47ff", null ],
    [ "ENC_SIGC", "_encoder_8h.html#a9d813425a24f4fd5bdc7a8d2165b41ca", null ],
    [ "EncoderInit", "_encoder_8h.html#ab6ba81db33f2315f1268754f9393ba74", null ],
    [ "getPosition", "_encoder_8h.html#ac5e58c5ab7e440cbd41029cba6d0aad6", null ],
    [ "getSpeed", "_encoder_8h.html#a2e4acdf2555e25e9230213f7b8c9a0d2", null ],
    [ "setZero", "_encoder_8h.html#a47affd1a10b589811fc4828c1a2e0c6d", null ]
];