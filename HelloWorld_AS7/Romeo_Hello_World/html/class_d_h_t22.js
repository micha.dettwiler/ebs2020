var class_d_h_t22 =
[
    [ "DHT22", "class_d_h_t22.html#a4f01bf12164b491c09bcb91dba50f12a", null ],
    [ "clockReset", "class_d_h_t22.html#a61b006bdef166780fcf3dbbc8dc56e33", null ],
    [ "getHumidity", "class_d_h_t22.html#ad4d4e2b26c509778579db68836757649", null ],
    [ "getHumidityInt", "class_d_h_t22.html#a8d3cba85c48b413e6826b6908bcdd640", null ],
    [ "getTemperatureC", "class_d_h_t22.html#af49d7227b2adf976e733ab04c5592bf5", null ],
    [ "getTemperatureCInt", "class_d_h_t22.html#a3bf287ddf9d82c91497c3a1723963a59", null ],
    [ "getTemperatureF", "class_d_h_t22.html#a485c4c515407bc9a41aac8ab9e449a02", null ],
    [ "readData", "class_d_h_t22.html#a61744883f0d5053d5bc93a2ed88ba84f", null ]
];