#ifndef _ENCODER_h
#define _ENCODER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

// Encoder an PortB
// ----------------
// PB0. .4...5...6...PB7
//		 C	 A   B
// Anschluesse
#define ENC_DDR DDRB // DDR
#define ENC_PIN PINB // Encoder-Eingaenge
#define ENC_SIGB (6)  // Signal B
#define ENC_SIGA (5)  // Signal A
#define ENC_SIGC (4)  // Signal C
// Encoder-Quadranten
#define ENC_Q1 (0x00)
#define ENC_Q2 (1<<ENC_SIGA)
#define ENC_Q3 (1<<ENC_SIGB)|(1<<ENC_SIGA)
#define ENC_Q4 (1<<ENC_SIGB)

void EncoderInit();
int32_t getPosition();
void setZero();
double getSpeed();
#endif