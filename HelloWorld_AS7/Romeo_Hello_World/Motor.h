/*
 * Motor.h
 *
 * Created: 05/01/2020 16:38:18
 *  Author: Mike
 */ 
#ifndef MOTOR1_H_
#define MOTOR1_H_

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#define LOCAL_TIMEZONE 0
#define COUNTS_PER_SECOND 64			// Counts of the encoder per second
#define MAX_COUNTS 1382400				// Maximum counts of the encoder (12h)

const int enableDrivePin = 5;			// Enable motor driver board connection to the digital interface port 5
const int invertDrivePin = 4;			// Enable motor driver board connection to the digital interface port 4
const int buttonPin= 8;					// Reset Pin -> the interrupt pin 0


#define K_P     0.01
#define K_I     0.002
#define K_D     0.00

void initMotor();
void encoderMotor();
void resetEncoder();
unsigned long TimeToImp(unsigned long h, unsigned long m, unsigned long s);
void MoveMotor(int h, int m, int s);

#endif /* MOTOR_H_ */