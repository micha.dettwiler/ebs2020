/*
 * GPS.cpp
 *
 * Created: 06.01.2020 21:16:37
 *  Author: Frutiger Jan
 */ 

#include "GPS.h"
#include <wire.h>
/**
 * @brief clears the buffer for an new GPS message
 *
 * @param[in] none
 *
 * @return void
 */
void GPS::resetGpsData()
{
	status='V';
	satelit=0;
}
/**
 * @brief clears the buffer for an new GPS message
 *
 * @param[in] none
 *
 * @return void
 */
void GPS::clearBuffer(void)									// function to clear buffer
{
	ptr = buffer;
	while(ptr < buffer + BUFFERSIZE)  *ptr++=0;			// fill with 0
	count = 0;											// set counter to zero
}
/**
 * @brief splitts the GPS message and save the important values
 *
 * @param[in] test:	char, newest char
 *
 * @return void
 */
void GPS::EncodeNMEATele(char text)
{	
	unsigned char c=  buffer[count] = text;	// write data into array
	if(count < BUFFERSIZE-1) count++;				// to avoid buffer overflow
	if(c == '$') {
		clearBuffer();								// start of frame found, reset buffer
		buffer[count++] = c;						// store start of frame
	}
	if(c == '*') // end of frame found, start conversion
	{
		frame =1;
	}
	if(frame) {											// full frame in buffer, so parse and decode
		frame = 0;
		ptr = strstr(buffer, NMEA_TIME);				// scan for GPRMC keyword
		if(ptr != NULL) {								// GPRMC keyword found, read time
			ptr += strlen(NMEA_TIME);
			t = atol(ptr);								// parse time value into hour, minute, second
			ptr+= strlen("094330.000,");
			status=*ptr;
			if (status == 'A')
			{
				time=t;				
				ptr = strstr(buffer, "*");
				ptr -= strlen("171210,,,A");
				t = atol(ptr);
				date=t;
			}
										
		} else {
			ptr2 = strstr(buffer, "$GPGGA,");				// scan for GPGGA keyword
			if(ptr2 != NULL)
			{
				ptr2 = strstr(buffer, "E,");				
				if(ptr2 == NULL)
				{
					ptr2 = strstr(buffer, "W,");
				}
				if(ptr2 != NULL)
				{
					ptr2+= strlen("W,1,");
					satelit= atol(ptr2);
				}
			}
			clearBuffer();	// clear buffer and start new
		}
	}
}
