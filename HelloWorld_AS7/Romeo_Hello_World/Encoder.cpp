﻿#include "Encoder.h"
#include "Motor.h"

int quadrant =-1,enc_error=0;
double speed=0;
long  lastPos=0, lastmilie =0;
long position=0;
int8_t newenc=0;
/**
 * @brief calc encoder position and monitor clock init
 *
 * @param[in] none
 *
 * @return void
 */
ISR(PCINT0_vect) {
	newenc = ENC_PIN & ((1 << ENC_SIGA) | (1 << ENC_SIGB));
	int8_t step, newq, q = quadrant;
	if (ENC_PIN & (1 << ENC_SIGC)) resetEncoder(); 
	switch (newenc){
		case ENC_Q1: newq = 0; break;
		case ENC_Q2: newq = 1; break;
		case ENC_Q3: newq = 2; break;
		case ENC_Q4: newq = 3; break;

	}
	if (q == -1) q = 1 + newq; // initialise
	step = (newq - q); // compute quadrant changes as a 2 bit integer!
	quadrant = newq; // save new quadrant
	if (step & 0x02) step |= 0xFC; // expand sign to 8 bit
	else step &= 0x03;
	// step is now an integer in the range -2..+1, with -2 as a quadrant leap
	if (step > -2) position += step;  // position changes by +/- 1 step
	else enc_error = 1; // -2 signals a possible loss of accuracy
}

int32_t getPosition()
{
	return position;
}

/**
 * @brief Encoder zurücksetzen
 *
 * @param[in] none
 *
 * @return void
 */
void setZero()
{
	position=0;
}
/**
 * @brief calc current speed
 *
 * @param[in] none
 *
 * @return speed:	double, current speed
 */
double getSpeed()
{
	speed= ((double)(position - lastPos) / (double)(millis()-lastmilie) *31.25);
	lastmilie=millis();
	lastPos=position;
	return speed;	
}
/**
 * @brief init interrupt
 *
 * @param[in] none
 *
 * @return void
 */
void EncoderInit()
{
	setZero();
	cli();
	PCICR |= 0b00000001; // Enables Port B Pin Change Interrupts PCINT0..7
	PCMSK0 |= B01110000;//(1 << ENC_SIGC) | (1 << ENC_SIGB) | (1 << ENC_SIGA); // PCINT5 and PCINT6
	sei();	
}