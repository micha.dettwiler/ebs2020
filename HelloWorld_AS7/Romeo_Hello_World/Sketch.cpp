﻿/**@file*/
#include <wire.h>
#include <hd44780.h>
#include <hd44780ioClass/hd44780_I2Cexp.h> // include i/o class header

#include "Romeo_keys.h"
#include "Time.h"
#include "Date.h"
#include "DHT22.h"
#include "GPS.h"
#include "Motor.h"
#include "Encoder.h"

hd44780_I2Cexp lcd;

#define NO_KEY 0
#define UP_KEY 1
#define LEFT_KEY 2
#define DOWN_KEY 3
#define RIGHT_KEY 4
#define OK_KEY 5
#define BK_KEY 6
#define X_KEY 10
// Debug == 1 Sekundentakt
// Debug == 2 10s Takt für DHT22
// Debug == 3 60 min Takt für GPS
// Debug == 4 100ms Takt Motor
#define debug 0
void setWorldTime(void );

// Setup Time and Date instance
Time localTime(00,5,0);
Time worldTime(0,0,0);
Date localDate(9,1,2020);
Date worldDate(0,0,0);
Time clockTime(0,0,0);

//================== DHT22 =====================================================================
// Data wire is plugged into port 7 on the Arduino
#define DHT22_PIN 11
// Setup a DHT22 instance
class DHT22 myDHT22(DHT22_PIN);
DHT22_ERROR_t errorCode;

/**
* @brief print the Temperatur on the lcd
*
*@param[in] Temp, Temperatur form the DHT22
*
* @return void
*/
void printTemp(float Temp)
{
	lcd.print("Temp.:  ");
	lcd.print(Temp/10,1);
	lcd.print(" \337C ");
}

/**
 * @brief print the Humi on the lcd
 *
 *@param[in] Humi, Humidity from the DHT22
 *
 * @return void
 */
void printHumi(float Humi)
{
	lcd.print("rH:     ");
	lcd.print(Humi/10,1);
	lcd.print(" \045  ");
}

/**
 * @brief Read all Intervall the new Temp and Humanity
 *
 * @param[in] AnDHT22, DHT22 lib form the Sensor
 *
 * @return 
 */
void getValues(DHT22 &AnDHT22)
{
	#define INTERVALDHT22 10000L
	static long target2 = INTERVALDHT22;
	if (millis() > target2)	{
		if(debug==2){Serial.println(millis());}
		target2 += INTERVALDHT22;
		errorCode = AnDHT22.readData();
	}
	#undef INTERVALDHT22
}

/**
 * @brief print the given Temperatur and Huminaty on two separate lines of an lcd
 *
 * @param[in] AnDHT22, DHT22 lib form the Sensor
 *
 * @return void
 */
void printTempAndHum(DHT22 &AnDHT22)
{
	lcd.setCursor(0,0);
	printTemp(AnDHT22.getTemperatureCInt());
	lcd.setCursor(0,1);
	printHumi(AnDHT22.getHumidityInt());
	lcd.setCursor(9,0);
}

/**
* @brief print a string with Temp and Humi to the LCD
*
* @param[in] key : int, user input
*
* @return key
*/
int TempHumiZeigen(int key)
{
	if (errorCode == 0) {
		printTempAndHum(myDHT22);
	} else {
		lcd.setCursor(0,0);
		lcd.print("Fehler!          ");
		lcd.setCursor(0,1); 
		lcd.print("Kein Sensorwert");
	}
	return key;
}
//================== GPS =======================================================================
bool gpsRead=0;
unsigned long start=0,noCon=0;
GPS gps;
/**
 * @brief read the UTC Time and Date form GPS
 *
 * @param[in] none
 *
 * @return void
*/
void GetGpsData()
{	
	if (!gpsRead) start = millis(); 
	gpsRead=(( millis() - start < 5000)||(gps.getGpsStatus()== 'V'));
	if (Serial1.available()) noCon=millis();	
	while(Serial1.available()) {						// as long as data is available on NMEA device
		gps.EncodeNMEATele(Serial1.read());	// write data into array
	}
	if ((millis() > (noCon+ 10000))&(!(gps.getGpsStatus()=='V')))
	{
		 gps.resetGpsData();
	}
	if (gps.getGpsStatus() == 'A')
	{
		long t=gps.GetGpsTime();
		localTime.setTime(((t/10000)%24),(t/100)%100,t%100);
		t=gps.GetGpsDate();
		localDate.setDate((t%100)+2000,t/100%100,t/10000);
		setWorldTime();
		start=0;
	}
} 
/**
 * @brief Synch the watch with the UTC Time from an GPS Modul
 *
 * @param[in] none
 *
 * @return void
 */
void getNewGpsData()
{
	#define INTERVALGPS 1800000L
	static long target2 = INTERVALGPS;
	if ((millis() > target2)|| gpsRead)	{
		if (millis() > target2) {target2 += INTERVALGPS;gps.resetGpsData();}
		if(debug==3){Serial.println(millis());}
		GetGpsData();
	}
	#undef INTERVALGPS
}
/**
* @brief print a string with the Satelit count to the LCD
*
* @param[in] key : int, user input
*
* @return key
*/
int printGPS(int key)
{
	GetGpsData();
	lcd.setCursor(0,1);
	lcd.print("Anz.: ");
	lcd.print(gps.getGpsSatelit());
	lcd.setCursor(10,1);
	(gps.getGpsStatus() == 'V') ? lcd.print("nIO"):lcd.print("IO ");
	
	//setZero();
	return key;
}

//============ UHR ===========================================================================
static int indexCity=0, indexCityold=0;
static bool Format=0;

/**
 * @brief A watch for the arduino, counting on the millis()
 *
 * requires a structure zeit_S with members ss,mm,hh,DD,MM,YYYY as int
 * frequent calls will result in counting the time. days-of-month and leap years are taken into account
 *
 * @return void
 */
void Watch()
{
#define INTERVAL 1000L
	static long target = INTERVAL;	
	if (millis() > target)	{
		if(debug==1){Serial.println(millis());}
		target += INTERVAL;
		if(localTime.Tick()) localDate.Tick();
		if(worldTime.Tick()) worldDate.Tick();
		clockTime.Tick();
		getValues(myDHT22);
		
	}
#undef INTERVAL
}

/**
 * @brief display the current time and date
 *
 * print a string "hh:mm:ss ALM" to the LCD
 * print the date to the lower line
 *
 * @param[in] key : int, user input
 *
 * @return key
 */
int ZeitZeigen(int key)
{
	char text[16];
	lcd.setCursor(0,0);
	if (Format)
		worldTime.printTime12(text);
	else
		worldTime.printTime24(text);
	lcd.print(text);
	lcd.setCursor(0,1);
	worldDate.print(text);
	lcd.print(text);
	return key;
}
/**
 * @brief set Time format to 12h
 *
 * @param[in] none
 *
 * @return void
 */
void setFormat12()
{
	Format=1;
}
/**
 * @brief set Time format to 24h
 *
 * @param[in] none
 *
 * @return void
 */
void setFomrat24()
{
	Format=0;
}
/**
 * @brief struct for the worldcities
 *
 *
 */
struct City
{
	const char * text;
	int HourZone;
};
const char strS[]  PROGMEM = "local";			/*0*/
const char strS0[] PROGMEM = "New-York";		/*1*/
const char strS1[] PROGMEM = "London";			/*2*/
const char strS2[] PROGMEM = "Paris";			/*3*/
const char strS3[] PROGMEM = "Tokio";			/*4*/
const char strS4[] PROGMEM = "Hongkong";		/*5*/
const char strS5[] PROGMEM = "Los Angeles";		/*6*/
const char strS6[] PROGMEM = "Chicago";			/*7*/
const char strS7[] PROGMEM = "Seoul";			/*8*/
const char strS8[] PROGMEM = "Br\xF5ssel";		/*9*/
const char strS9[] PROGMEM = "Washington";		/*10*/
const char strS10[] PROGMEM = "Singapur";		/*11*/
const char strS11[] PROGMEM = "Sydney";			/*12*/

struct City Citys[] =
{
	/*0*/	{strS	,1 },
	/*1*/	{strS0	,-5 },
	/*2*/	{strS1	,0 },
	/*3*/	{strS2	,1 },
	/*4*/	{strS3	,9 },
	/*5*/	{strS4	,8 },
	/*6*/	{strS5	,-8 },
	/*7*/	{strS6	,-6 },
	/*8*/	{strS7	,9 },
	/*9*/	{strS8	,1 },
	/*10*/	{strS9	,-5},
	/*11*/	{strS10	,8},
	/*12*/	{strS11	,10}
};
static char strtemp[20]; /// temporary variable for access to PROGMEM strings
#define ROM(a) strcpy_P(strtemp, a) /// macro hiding the strcpy_P to the temp variable 'strtemp'

/**
 * @brief Add the Timezone to the local Time and date and save it in the World- Time and Date
 *
 * @param[in] none
 *
 * @return void
 */
void setWorldTime()
{
	worldTime.Change(localTime);	
	int day= worldTime.addHour(Citys[indexCity].HourZone); 
	worldDate.Change(localDate);
	worldDate.addDay(day);
	indexCityold=indexCity;
	clockTime.Change(localTime);
	clockTime.addHour(Citys[0].HourZone);
}
	
/**
 * @brief Change the city to the next
 *
 * @param[in] none
 *
 * @return void
 */
void setHourZoneUp()
{
	(indexCity >= 12) ? indexCity=0 : indexCity+=1;
}
/**
 * @brief Change the city to the previous
 *
 * @param[in] none
 *
 * @return void
 */
void setHourZoneDown()
{
	(indexCity < 0) ? indexCity=11 : indexCity-=1;
}
/**
 * @brief choose the actual city
 *
 * @param[in] key : int, user input
 *
 * @return key
 */
int EinstellenZone(int key)
{	
	lcd.setCursor(0,1);
  	lcd.print(ROM(Citys[indexCity].text));
	char text[16];
	sprintf(text,"Z:%02i",Citys[indexCity].HourZone);
	lcd.setCursor(12,1);
	lcd.print(text);
	return key;
}
/**
 * @brief change the local Timezone 
 *
 * @param[in] none
 *
 * @return void
 */
void lowerZoneHour()
{
	Citys[indexCity].HourZone--;
	lcd.setCursor(0,1);
	lcd.print(ROM(Citys[indexCity].text));
	char text[16];
	sprintf(text,"Z:%02i",Citys[indexCity].HourZone);
	lcd.setCursor(12,1);
	lcd.print(text);
}
/**
 * @brief change the local Timezone 
 *
 * @param[in] none
 *
 * @return void
 */
void upperZoneHour()
{
	Citys[indexCity].HourZone++;
	lcd.setCursor(0,1);
	lcd.print(ROM(Citys[indexCity].text));
	char text[16];
	sprintf(text,"Z:%02i",Citys[indexCity].HourZone);
	lcd.setCursor(12,1);
	lcd.print(text);
}
/**
 * @brief change the local Timezone 
 *
 * @param[in] key : int, user input
 *
 * @return key
 */
int EinstZeitZone(int key)
{
	lcd.setCursor(14,1);	
	lcd.blink();
	//Unerwünschte Tasten unterdrücken
	if (key != LEFT_KEY & key != RIGHT_KEY & key != OK_KEY){
		key= NO_KEY;
	}
	if (indexCity != 0){key=OK_KEY;}
	return key;
}

/**
 * @brief modifies the struct datum
 *
 * on each call, exactly one user input is processed
 *
 * @param[in] key: int, user input
 *
 * input UP, DOWN, RIGHT, LEFT_KEY is processed and returned as X_KEY 
 *
 * @return int processed user input,  all except the above mentioned is returned
 */
int ChangeDate(Date &z, int key)
{
	switch(key){
	case LEFT_KEY:
		if (z.getSet() > 1) z.ChangeSet(-1);
		key = X_KEY;
		break;
	case RIGHT_KEY:
		if (z.getSet() < 3) z.ChangeSet(+1);
		key = X_KEY;
		break;
	}

	//! modify YEAR
	if(z.getSet() == 3){
		switch(key){
		case UP_KEY:
			if(z.GetYear() <= 2099) z.ChangeYear(+1);
			key=X_KEY;
			break;
		case DOWN_KEY:
			if(z.GetYear()>=1900) z.ChangeYear(-1);
			key=X_KEY;
			break;
		}
	}
	//! modify MONTH
	else if(z.getSet() == 2){
		switch(key){
			case UP_KEY:
			z.ChangeMonth(+1);
			key=X_KEY;
			break;
		case DOWN_KEY:
			z.ChangeMonth(-1);
			key=X_KEY;
			break;
		}
	}
	//! modify DAY
	else{
		switch(key){
		case UP_KEY:
			z.ChangeDay(+1);
			key=X_KEY;
			break;
		case DOWN_KEY:
			z.ChangeDay(-1);
			key=X_KEY;
			break;
		}
	}
	return key;
}
/**
 * @brief modifies the struct zeit 
 *
 * on each call, exactly one user input is processed 
 *
 * @param[in] key: int, user input
 *
 * input UP, DOWN, RIGHT, LEFT_KEY is processed and returned as NO_KEY 
 *
 * @return int processed user input,  all except the four above mentioned is returned
*/
int ChangeTime(Time &z, int key)
{
	switch(key){
	case RIGHT_KEY:
		z.setSet(2);
		key = X_KEY;
		break;
	case LEFT_KEY:
		z.setSet(1);
		key = X_KEY;
		break;
	}
	
	if(z.GetSet() == 1){
		//! change hours
		switch(key){
		case UP_KEY:
			z.ChangeHour(+1);
			key=X_KEY;
			break;
		case DOWN_KEY:
			z.ChangeHour(-1);
			key=X_KEY;
			break;
		}
	}
	else if(z.GetSet() == 2){
		//! change minutes
		switch(key){
		case UP_KEY:
			z.ChangeMin(+1);
			key=X_KEY;
			break;
		case DOWN_KEY:
			z.ChangeMin(-1);
			key=X_KEY;
			break;
		}
	}
	return key;
} 

/**
 * @brief change the current date
 *
 * hand the job down to changedate()
 *
 * @param[in] key : int, user input
 *
 * @return key
 */
int UhrAendern(int key)
{
	if (localDate.getSet() < 4) {
		key = ChangeDate(localDate,key);
		if (key == OK_KEY){localDate.setSet(4); key=X_KEY;}		
	} else {
		key = ChangeTime(localTime,key);
		if (key == OK_KEY){localDate.setSet(3);localTime.setSet(1);}	
	}
	
	worldTime.Change(localTime);
	worldTime.addHour(Citys[indexCity].HourZone);
	worldDate.Change(localDate);
	clockTime.Change(localTime);
	clockTime.addHour(Citys[0].HourZone);
	ZeitZeigen(key);
	return key;
}

//============ Positionieren ===================================================================

void position()
{
	#define INTERVALMOTOR 100L
	static long targetMotor = INTERVALMOTOR;
	if (millis() > targetMotor)	
	{
		if(debug==4) Serial.println(millis());
		targetMotor += INTERVALMOTOR;
		MoveMotor(clockTime.GetHours(),clockTime.GetMinutes(),clockTime.GetSeconds());
	}
	#undef INTERVALMOTOR
}


//============= GUI =============================================================================
typedef int (*PFA_T)(int key);
typedef void (*PFP_T)(void);
/**
 * @brief struct for one entry in the menu
 *
 *
 */
struct FSM_TAG
{
	//! text to print on the upper line of the lcd
	const char * text1;
	//! these fife members contain the number of the menu that is to be jumped to if the corresponding key is hit 
	int up, left, down, right, ok, okl; 
	//! function active will be called as long as the menu is active
	PFA_T active; 
	 //! function goright will be called upon a hit on the 'right' key
	PFP_T goright;
	//! function goright will be called upon a hit on the 'right' key
	PFP_T goleft;
	 //! function positive will be called upon a hit on the 'ok' key
	PFP_T positive;
};

const char str0[] PROGMEM = ""; // wird erst angezeigt wenn über taste auf Seite gewechselt wird!
const char str1[] PROGMEM = "";
const char str2[] PROGMEM = "Satelliten";
const char str3[] PROGMEM = "Weltstadt:";
const char str4[] PROGMEM = "Weltstadt:";
const char str5[] PROGMEM = "";
const char str6[] PROGMEM = "";
const char str7[] PROGMEM = "";
const char str8[] PROGMEM = "";

const struct FSM_TAG watchmenu[] =
{
	//            ^   <   v   >  ok	 OK		active			right			left			ok
	/*0*/ {str0,  1, -1,  3, -1, -1,  5,	ZeitZeigen,		setFomrat24,	setFormat12,	NULL},
	/*1*/ {str1,  2, -1,  0, -1, -1, -1,	TempHumiZeigen,	NULL,			NULL,			NULL},
	/*2*/ {str2,  3,  2,  1,  2,  1, -1,	printGPS,		NULL,			NULL,			NULL},
	/*3*/ {str3,  0,  3,  2,  3,  0,  4,	EinstellenZone,	setHourZoneUp,	setHourZoneDown,setWorldTime},
	/*4*/ {str4,  4,  4,  4,  4,  3, -1,	EinstZeitZone,	upperZoneHour,	lowerZoneHour	,NULL},
	/*5*/ {str5, -1, -1, -1, -1,  0, -1,	UhrAendern,		NULL,			NULL,			NULL},
	/*6*/ {str6, -1,  6, -1,  6,  5, -1,	NULL,			NULL,			NULL,			NULL},
	/*7*/ {str7,  5,  7,  0,  8,  7, -1,	NULL,			NULL,			NULL,			NULL},
	/*8*/ {str8, -1,  8, -1,  8,  7, -1,	NULL,			NULL,			NULL,			NULL},
	/*9*/ {str0, -1, -1, -1, -1, -1, -1,	NULL,			NULL,			NULL,			NULL}
};
static int menu = 0, newmenu = 0;
static int input = 0; /// user input


//============ SETUP ===========================================================================
/**
 * \brief arduino setup
 * 
 * called once at program startup
 * \return void
 */
void setup()
{
	Serial1.begin(9600); // Serial Port für GPS Modul aktivieren
	lcd.begin(16, 2);
	lcd.noBacklight();
	lcd.noCursor();
	lcd.home();
	lcd.print(F("Romeo II Watch"));	
	GetGpsData();	
	initMotor();
	setWorldTime();	
	while(millis() < 2000);
	myDHT22.readData(); // init read, damit von beginn an korrekte werte angezeigt werden
	lcd.clear();
}

//============= Ablauf =========================================================================
/**
 * \brief arduino loop
 * called repeatedly in an endless loop
 * 
 * \return void
 */
void loop()
{
	// Werte aktuallisieren
	Watch();
	position();
	getNewGpsData();
	// Tasten druck auswerten
	input = getkey();
	if (watchmenu[menu].active) input = watchmenu[menu].active(input);
	//! cyclic call to active function as long as menu n is active
	switch(input) {
		default:
		case X_KEY:
		newmenu = watchmenu[menu].okl;
		break;
		case NO_KEY:
		newmenu = -1; // no changes made
		break;
		case UP_KEY:
		indexCity=indexCityold;
		lcd.clear();
		newmenu = watchmenu[menu].up;
		break;
		case LEFT_KEY:
		lcd.clear();
		if(watchmenu[menu].goleft!= NULL) {
			//! call go right function with new value
			watchmenu[menu].goleft();
		}
		newmenu = watchmenu[menu].left;
		break;
		case DOWN_KEY:
		indexCity=indexCityold;
		lcd.clear();
		newmenu = watchmenu[menu].down;
		break;
		case RIGHT_KEY:
		lcd.clear();
		if(watchmenu[menu].goright != NULL) {
			//! call go right function with new value
			watchmenu[menu].goright();
		}
		newmenu = watchmenu[menu].right;
		break;
		case OK_KEY:
		lcd.clear();
		lcd.noBlink();
		if(watchmenu[menu].positive != NULL) {
			//! call positive answer function with new value
			watchmenu[menu].positive();
		}
		newmenu = watchmenu[menu].ok;
		break;
		case BK_KEY:
		lcd.clear();
		lcd.noCursor();
		lcd.noBlink();
		newmenu = 0;
		break;
	}
	if(newmenu >= 0){
		menu = newmenu;
		lcd.setCursor(0, 0);
		lcd.print(ROM(watchmenu[menu].text1));
	}
	
}
