/*
 * IncFile1.h
 *
 * Created: 06.01.2020 21:11:15
 *  Author: Frutiger Jan
 */ 


#ifndef GPS3_H_
#define GPS3_H_

class GPS
{
	private:
	#define NMEA_TIME "GPRMC,"
	#define BUFFERSIZE 68
	char buffer[BUFFERSIZE];							// buffer for data from NMEA device
	int count=0,satelit=0,frame = 0;
	char* ptr;
	char* ptr2;
	long t,time,date;
	char status='V';
	void clearBuffer(void);
	public:	
	void EncodeNMEATele(char text);
	char getGpsStatus(){return status;};
	long getGpsSatelit(){return satelit;};
	long GetGpsTime(){return time;};
	long GetGpsDate(){return date;};
	void resetGpsData();	
};


#endif /* GPS_H_ */