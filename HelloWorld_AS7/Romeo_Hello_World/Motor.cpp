#include "Motor.h"
#include "Encoder.h"
#include "PID_v1.h"

int targetSpeed = 255;					// Target speed for the motor
volatile  long encodeCount;		// Counter for the mechanical clock
long targetCount;				// Target value for the mechanical clock
bool clockEnable = 0;				// Enable the clock
bool ref =0,ref1=0,ref2=0;
double speed2=0;
//! Parameters for regulator
//struct PID_DATA pidData;
double referenceValue, measurementValue, inputValue;
unsigned long test,test1,test2;

//Specify the links and initial tuning parameters
PID myPID(&measurementValue, &inputValue, &referenceValue,0.1,0,0,P_ON_E, DIRECT);

void initMotor()
{	
	EncoderInit();
	int hour1=0;
	int minute1=10;
	int second1=0;
	
	pinMode(invertDrivePin, OUTPUT);
	pinMode(enableDrivePin, OUTPUT);
	pinMode(buttonPin, INPUT);
	
	//turn the PID on
	myPID.SetMode(AUTOMATIC);
	clockEnable = true;							// Enable the mechanical clock
	encodeCount = 0;
	ref=0;
	
	targetCount = (MAX_COUNTS + COUNTS_PER_SECOND);		// Convert seconds to counter value
}

void resetEncoder()
{
	setZero();
	targetCount= (MAX_COUNTS + COUNTS_PER_SECOND);
	ref=1;
}

unsigned long TimeToImp(unsigned long h, unsigned long m, unsigned long s)
{
	test =(((h % 12) * 3600)+(m * 60)+ s)* COUNTS_PER_SECOND;		// Convert current time to seconds
	return test;
}

void MoveMotor(int h, int m, int s)
{
	if(ref==1)
	{
		if(((s%5) == 0)||(ref1==0) ) targetCount= TimeToImp(h,m,s); ref1=1;
	}
	encoderMotor();	
}


void encoderMotor()
{
	encodeCount= getPosition();
	speed2=getSpeed();
	if ((encodeCount > (targetCount-32))&& (ref1==1) && (ref2==0) && (speed2==0))
	{
		myPID.SetTunings(0.3,0,0); //0.25,0,0.15 / 0.3,0.1,0.03
		ref2=1;
	}
	measurementValue=(double)encodeCount; 
	referenceValue=(double)targetCount;
	myPID.Compute();	
	
	if (abs(targetCount- encodeCount) < 64) 
	{
		inputValue=0;
	}
	
	if (clockEnable)
	{ 
		digitalWrite(invertDrivePin, (int)inputValue > 0 ? LOW : HIGH);
		analogWrite(enableDrivePin, (int)inputValue > 0 ? (int)inputValue : - (int)inputValue);
	}
	else
	{
		digitalWrite(enableDrivePin, LOW);
	}
}