

#ifndef _DATE22_h
#define _DATE22_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Date {
	int YYYY_;
	int MM_;
	int DD_;
	int set_;
	public:
	Date(int d, int m, int y){DD_=d; MM_=m;YYYY_=y; set_ = 3;};

	void Tick();
	int DaysOfMonth();
	int GetYear();
	int GetMonth();
	int GetDay();
	int getSet();
	void ChangeYear(int y);
	void ChangeMonth(int m);
	void ChangeDay(int d);
	void ChangeSet(int s);
	void setSet(int s);
	void addDay(int d);
	void Change(Date &z);
	void setDate(int y, int m, int d);
	void print(char *text);
};
#endif